export const colors = {
    // default: '#e74c3c', //red
    primary: '#1FCC79', //green
    secondary: '#FF5842', //oranye
    mainText: '#2E3E5C', //dark
    secondaryText:'#9FA5C0',
    outline: '#D0DBEA',//lightgrey
    form:'#F4F5F7',//white dark

    text: {
        default: '#7e7e7e'//grey
    }
};
