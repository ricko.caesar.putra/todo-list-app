import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Alert, StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import CardAllTask from '../../components/atoms/CardAllTask';
import { FloatingAction } from "react-native-floating-action";
import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../../utils';
import { Input } from '../../components';

const ActiveTask = ({ navigation }) => {
    const [allTask, setAllTask] = useState([]);
    const [valueNewTask, setValueNewTask] = useState('')
    const [showAddTask, setShowAddTask] = useState(false)
    const [token, setToken] = useState('')
    const [isWaiting, setIswaiting] = useState(true)
    const actions = [
        {
            text: "Add Task",
            icon: <Icon name='ios-add-circle' size={20} color={'red'} />,
            name: "AddTask",
            position: 1
        },]

    const baseUrl = 'https://api-nodejs-todolist.herokuapp.com';
    const headers = {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
    }

    useEffect(() => {
        readData()
        getData();
        setTimeout(() => {
            setIswaiting(false)
        }, 2000);
    }, [token])

    const readData = async () => {
        try {
            const tokenAsync = await AsyncStorage.getItem(`@token`)

            if (tokenAsync !== null) {
                setToken(tokenAsync)
            }
        } catch (e) {
            alert('Failed to fetch the data from asynch storage')
        }
    }
    
    const getData = () => {

        axios({
            method: 'get',
            url: `${baseUrl}/task?completed=false`,
            headers
        })
            .then(res => {
                setAllTask(res.data.data);
            })
            .catch(error => {
                console.log(error.response)
            })
    }

    const deleteItem = (task) => {
        axios({
            method: 'delete',
            url: `${baseUrl}/task/${task._id}`,
            headers
        })
            .then(res => getData())
            .catch(error => {
                console.log(error.response)
            })
    }

    const changeStatus = (task) => {
        axios({
            method: 'put',
            url: `${baseUrl}/task/${task._id}`,
            headers,
            data: { "completed": !task.completed },
        })
            .then(res => getData())
            .catch(error => {
                console.log(error.response)
            })
    }

    const addItem = (valueNewTask) => {
        axios({
            method: 'post',
            url: `${baseUrl}/task`,
            headers,
            data: { "description": valueNewTask },
        })
            .then(res =>
                getData())
            .catch(error => {
                console.log(error.response)
            })

        setShowAddTask(false)
        setValueNewTask('')
    }

    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity style={styles.barAtas} onPress={() => getData()}>
                <Text style={styles.textAtas}>Active Task</Text>
            </TouchableOpacity>

            {showAddTask ?
                <View style={styles.addTask}>
                    <TouchableOpacity onPress={
                        () => setShowAddTask(false)
                    }>
                        <Icon name='ios-close-circle' size={50} color={'red'} />
                    </TouchableOpacity>
                    <Input
                        type="AddTask"
                        placeholder="Add Task"
                        value={valueNewTask}
                        onChangeText={(value) => setValueNewTask(value)}
                    />
                    <TouchableOpacity onPress={
                        () => addItem(valueNewTask)
                    }>
                        <Icon name='ios-add-circle' size={50} color={'red'} />
                    </TouchableOpacity>
                </View> :
                null}
            {isWaiting ?
                <ActivityIndicator style={{ marginTop: 10 }} size='large' color={'red'} /> :
                <ScrollView style={{ paddingHorizontal: 10 }}>
                    {allTask.map(task => {
                        return <CardAllTask
                            key={task._id}
                            completed={task.completed}
                            description={task.description}
                            owner={task.owner}
                            createdAt={task.createdAt}
                            updatedAt={task.updatedAt}
                            onPress={() => changeStatus(task)}
                            onDelete={() => Alert.alert(
                                'peringatan',
                                'anda yakin akan menghapus user ini?',
                                [
                                    {
                                        text: 'Tidak',
                                        onPress: () => console.log('Tidak')
                                    },
                                    {
                                        text: 'Ya',
                                        // onPress: () => null
                                        onPress: () => deleteItem(task)
                                    }
                                ])} />
                    })}
                    <View style={{ height: 100 }} />
                </ScrollView>}
            <FloatingAction
                actions={actions}
                onPressItem={name => {
                    { name === "AddTask" ? setShowAddTask(true) : null };
                }}
            />

        </View>
    )
}

export default ActiveTask

const styles = StyleSheet.create({
    barAtas: {
        height: 70,
        backgroundColor: colors.primary,
        borderRadius: 35,
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textAtas: {
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white'
    },
    floatingIcon: {
        // position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        right: 0,
        bottom: 0,
        elevation: 20,
    },
    addTask: {
        flexDirection: 'row',
        marginHorizontal: 20,
        marginBottom: 10,
        justifyContent: 'center'
    }
})
