import React, { useEffect } from 'react'
import { Image } from 'react-native';
import { StyleSheet, Text, View } from 'react-native'
import { AnimationList, AnimationList2, OnboardingImage } from '../../assets';
import { Button } from '../../components';
import { colors } from '../../utils';
import LottieView from 'lottie-react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';


const SplashScreen = (props) => {
    useEffect(() => {
        setTimeout(() => {
            getData();
        }, 2000);
    });

    const getData = async () => {
        try {
          const jsonValue = await AsyncStorage.getItem('@token')
          props.navigation.navigate(jsonValue?'BotomTab':'OnBoarding')            
        } catch(e) {
          console.warn(e)
        }
      }
    
    return (
        <View style={{flex:1, alignItems:'center'}}>
            <View style={{height:50}}/>
            <LottieView source={AnimationList2} style={{width:300,height:300,marginTop:30}} autoPlay loop/>
            {/* <Image style={styles.imageFiturUtama} source={OnboardingImage} height={200} width={200}/> */}
            <View style={{height:50}}/>
            <Text style={styles.text1}>To-Do List</Text>
            <View style={{height:10}}/>
            <Text style={styles.text2}>Let’s join us</Text>
            <View style={{flex:1}}/>
        </View>
    )
}

export default SplashScreen

const styles = StyleSheet.create({
    gambar:{
        marginTop:35,
    },
    text1:{
        fontSize:22,
        fontWeight:'bold',
        alignSelf:'center',
        color:colors.mainText
    },
    text2:{
        fontSize:22,
        fontWeight:'bold',
        alignSelf:'center',
        color:colors.secondaryText

    },
    imageFiturUtama:{
        width:420,
        height:406
    },
    garisBawah:{
        marginTop:35,
        width:134,
        height:5,
        backgroundColor:'black',
        borderRadius:3
    }

})
