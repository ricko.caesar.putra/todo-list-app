import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Alert, StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import CardAllTask from '../../components/atoms/CardAllTask';
import { FloatingAction } from "react-native-floating-action";
import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../../utils';
import { Button, Input } from '../../components';
import CardProfile from '../../components/atoms/CardProfile';
import { connect } from 'react-redux';
import CardEditProfile from '../../components/atoms/CardEditProfile';

const Profile = (props) => {
    const [allTask, setAllTask] = useState({});
    const [valueNewTask, setValueNewTask] = useState('')
    const [isLoading, setIsLoading] = useState(true)
    const [token, setToken] = useState('')
    const [isEdit,setIsEdit] = useState(false)
    const [activeTask,setActiveTask]=useState([])
    const [openTask,setOpenTask]=useState([])

    const baseUrl = 'https://api-nodejs-todolist.herokuapp.com';
    // const headers = {
    //     'Authorization': `Bearer ${token}`,
    // }
    const headers = {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
    }

    useEffect(() => {
        readData()
        getData();
        getAllTask();
        getOpenTask();
        setTimeout(() => {
            setIsLoading(false)
        }, 1000);
    }, [token]);


    const readData = async () => {
        try {
            const tokenAsync = await AsyncStorage.getItem(`@token`)

            if (tokenAsync !== null) {
                setToken(tokenAsync)
            }
        } catch (e) {
            alert('Failed to fetch the data from asynch storage')
        }
    }

    const getData = async () => {
        await axios({
            method: 'get',
            url: `${baseUrl}/user/me`,
            headers
        })
            .then(res => {
                setAllTask(res.data);
                setIsEdit(false)
            })
            .catch(error => {
                console.log(error)
            })
    }

    const getAllTask = () => {
        axios({
            method: 'get',
            url: `${baseUrl}/task`,
            headers
        })
            .then(res => {
                setActiveTask(res.data.count);
            })
            .catch(error => {
                console.log(`===all task===`)
                console.log(error)
            })

    }

    const getOpenTask = () => {

        axios({
            method: 'get',
            url: `${baseUrl}/task?completed=false`,
            headers
        })
            .then(res => {
                setOpenTask(res.data.count);
            })
            .catch(error => {
                console.log(error.response)
            })
    }

    const _Delete = async () => {
        await axios({
            method: 'delete',
            url: `${baseUrl}/user/me`,
            headers
        })
            .then(res => {
                setAllTask(res.data);
            })
            .catch(error => {
                console.log(error.response)
            })
    }

    const _logout = async () => {
        try {
            props.dispatchLogout();
            console.warn('dispatch redux logout success')
        } catch (e) {
            console.warn(`dispatch redux logout failed`)
        }

        try {
            await AsyncStorage.clear()
            console.warn('asynch Storage successfully cleared!')
        } catch (e) {
            console.warn('Failed to clear the async storage.')
        }

        try {
            await axios({
                method: 'post',
                url: `${baseUrl}/user/logout`,
                headers
            })
            console.warn('post axios logout success')
        } catch (e) {
            console.warn('post axios logout failed')
        }

        props.navigation.navigate('OnBoarding')

    }

    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity style={styles.barAtas} onPress={() => { 
                getData()
                getAllTask();
                getOpenTask();
                }}>
                <Text style={styles.textAtas}>Profile</Text>
            </TouchableOpacity>
            {isLoading ?
                <ActivityIndicator style={{ marginTop: 10 }} size="large" color={'red'} />
                :
                <View style={{ paddingHorizontal: 10 }}>
                    {isEdit?
                    <CardEditProfile
                        token={token}
                        key={allTask._id}
                        age={allTask.age}
                        name={allTask.name}
                        email={allTask.email}
                        createdAt={allTask.createdAt}
                        updatedAt={allTask.updatedAt}
                    />:
                    <CardProfile
                    key={allTask._id}
                    age={allTask.age}
                    name={allTask.name}
                    email={allTask.email}
                    createdAt={allTask.createdAt}
                    updatedAt={allTask.updatedAt}
                    activeTask={activeTask}
                    openTask={openTask}
                />}

                    <View style={{ height: 20 }} />
                </View>
            }

            <View style={{ alignItems: 'center' }}>
                <Button title='EDIT' type="default" onPress={() => setIsEdit(!isEdit)} />
                <View style={{ height: 5 }} />

                <Button title='DELETE' type="default" onPress={() => Alert.alert(
                    'peringatan',
                    'anda yakin akan menghapus user ini?',
                    [
                        {
                            text: 'Tidak',
                            onPress: () => console.log('Tidak')
                        },
                        {
                            text: 'Ya',
                            // onPress: () => null
                            onPress: () => _Delete()
                        }
                    ])} />
                <View style={{ height: 5 }} />
                <Button title='LOGOUT' type="default" onPress={() => _logout()} />
            </View>

        </View>
    )
}

function mapStateToProps(state) {
    return {
        authStore: state.authStore,
        isLoading: state.authStore.isLoading
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchLogout: () => dispatch({ type: 'LOGOUT' }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)

const styles = StyleSheet.create({
    barAtas: {
        height: 70,
        backgroundColor: colors.primary,
        borderRadius: 35,
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textAtas: {
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white'
    },
    floatingIcon: {
        // position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        right: 0,
        bottom: 0,
        elevation: 20,
    },
    addTask: {
        flexDirection: 'row',
        marginHorizontal: 20,
        marginBottom: 10,
        justifyContent: 'center'
    }
})
