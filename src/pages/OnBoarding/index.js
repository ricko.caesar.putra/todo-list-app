import React from 'react'
import { Image } from 'react-native';
import { StyleSheet, Text, View } from 'react-native'
import { AnimationList, AnimationList2, OnboardingImage } from '../../assets';
import { Button } from '../../components';
import { colors } from '../../utils';
import LottieView from 'lottie-react-native';


const OnBoarding = ({ navigation }) => {
    return (
        <View style={{flex:1, alignItems:'center'}}>
            <View style={{height:10}}/>
            <LottieView source={AnimationList2} style={{width:300,height:300,marginTop:30}} autoPlay loop/>
            {/* <Image style={styles.imageFiturUtama} source={OnboardingImage} height={200} width={200}/> */}
            <View style={{height:50}}/>
            <Text style={styles.text1}>To-Do List</Text>
            <View style={{height:10}}/>
            <Text style={styles.text2}>Let’s join us</Text>
            <View style={{flex:1}}/>
            <Button title='Sign in' type="default" onPress={() => navigation.navigate('SignIn')} />
            <View style={{height:10}}/>
            <Button title='Sign up' type="default" onPress={() => navigation.navigate('SignUp')} />
            <View style={styles.garisBawah}/>
        </View>
    )
}

export default OnBoarding

const styles = StyleSheet.create({
    gambar:{
        marginTop:35,
    },
    text1:{
        fontSize:22,
        fontWeight:'bold',
        alignSelf:'center',
        color:colors.mainText
    },
    text2:{
        fontSize:22,
        fontWeight:'bold',
        alignSelf:'center',
        color:colors.secondaryText

    },
    imageFiturUtama:{
        width:420,
        height:406
    },
    garisBawah:{
        marginTop:35,
        width:134,
        height:5,
        backgroundColor:'black',
        borderRadius:3
    }

})
