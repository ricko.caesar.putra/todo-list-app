import React, { useEffect, useState } from 'react'
import { Dimensions } from 'react-native';
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button, Input, ValPassword } from '../../components';
import { colors } from '../../utils';
const { height, width } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons'
import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import fetchLogin from '../../stores/actions/authActions';
import { connect } from 'react-redux';

const SignIn = (props) => {
    const [notShowPassword, setnotShowPassword] = useState(true)
    const [email, setEmail] = useState('Ricko10@gmail.com');
    const [password, setPassword] = useState('1234567');
    const [valNum, setValNum] = useState('grey')
    const [valLength, setValLength] = useState('grey')

    const handleLogin = async () => {
        try {
            props.dispatchLogin(email, password);

        } catch (e) {
            alert(e)
            console.error(e);
        }
    };

    useEffect(() => {
        const { navigation } = props;
        if (props.authStore.payload.token && props.authStore.payload.token !== '') {
            // console.warn(props.authStore.payload.token)
            const saveData = async () => {
                try {
                    await AsyncStorage.setItem('@token', props.authStore.payload.token)
                    console.warn(`token ${props.authStore.payload.token}successfully saved to async storage`)
                } catch (e) {
                    console.warn('Failed to save the data to the async storage')
                }
            }
            saveData()
            navigation.navigate('BotomTab');
        }
    }, [props.authStore.payload])

    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <View style={styles.space(55)} />
            <Text style={styles.text1}>Login</Text>
            <View style={styles.space(32)} />
            <Text style={styles.text2}>Please enter your account here</Text>
            <View style={styles.space(32)} />
            <Input
                type="email"
                placeholder="Email"
                value={email}
                onChangeText={(value) => setEmail(value)}
            />
            <View style={styles.space(5)} />
            <Input
                type="password"
                placeholder="Password"
                notShowPassword={notShowPassword}
                onPress={() => setnotShowPassword(!notShowPassword)}
                value={password}
                onChangeText={(value) => {
                    setPassword(value)
                    { value.length >= 6 ? setValLength('green') : setValLength('grey') }
                    { value.match(/[0-9]/g) ? setValNum('green') : setValNum('grey') }
                }}
            />
            <View style={styles.space(24)} />
            <ValPassword valLength={valLength} valNum={valNum} />
            <View style={{ flex: 1 }} />
            <Button title='Sign in' type="default" onPress={() => handleLogin({ email, password })} />
            <View style={styles.garisBawah} />
        </View>
    )
}

function mapStateToProps(state) {
    return {
        authStore: state.authStore,
        isLoading: state.authStore.isLoading
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchLogin: (email, password) => dispatch(fetchLogin(email, password)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)

const styles = StyleSheet.create({
    gambar: {
        marginTop: 56,
    },
    text1: {
        fontSize: 22,
        fontWeight: 'bold',
        alignItems: 'center',
        color: colors.mainText
    },
    text2: {
        fontSize: 15,
        fontWeight: '300',
        alignItems: 'center',
        color: colors.secondaryText

    },
    text3: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.mainText
    },
    tombol: {
        alignItems: 'center'
    },
    space: value => {
        return {
            height: value,
        };
    },
    garisBawah: {
        marginTop: 35,
        width: 134,
        height: 5,
        backgroundColor: 'black',
        borderRadius: 3
    },
    signup: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.primary,
        marginLeft: 10
    }

})
