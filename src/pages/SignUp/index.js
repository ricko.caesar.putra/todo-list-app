import React, { useState } from 'react'
import { Dimensions } from 'react-native';
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Button, Input, ValPassword } from '../../components';
import { colors } from '../../utils';
const { height, width } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons'
import axios from 'axios'


const SignUp = ({ navigation }) => {
    const [notShowPassword, setnotShowPassword] = useState(true)
    const [valueName, setValueName] = useState('')
    const [valuePassword, setValuePassword] = useState('')
    const [valueEmail, setValueEmail] = useState('')
    const [valueAge, setValueAge] = useState('')
    const [valNum, setValNum] = useState('grey')
    const [valLength, setValLength] = useState('grey')

    const formRegister =
    {
        "name": valueName,
        "email": valueEmail,
        "password": valuePassword,
        "age": valueAge,
    }

    const sendData = async () => {
        try {
             axios.post('https://api-nodejs-todolist.herokuapp.com/user/register', formRegister)
            .then(function (response) {
                console.warn(response)
                if (response.data != null) {
                    navigation.navigate('SignIn')
                } else {
                    alert('error')
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        } catch (error) {
            console.error(error)
        }
    }

    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <View style={styles.space(55)} />
            <Text style={styles.text1}>Register</Text>
            <View style={styles.space(32)} />
            <Text style={styles.text2}>Please enter your account here</Text>
            <View style={styles.space(32)} />
            <Input
                type="name"
                placeholder="name"
                value={valueName}
                onChangeText={(value) => setValueName(value)}
            />
            <View style={styles.space(5)} />
            <Input
                type="email"
                placeholder="Email"
                value={valueEmail}
                onChangeText={(value) => setValueEmail(value)}
            />
            <View style={styles.space(5)} />
            <Input
                type="password"
                placeholder="Password"
                notShowPassword={notShowPassword}
                onPress={() => setnotShowPassword(!notShowPassword)}
                value={valuePassword}
                onChangeText={(value) => {
                    setValuePassword(value)
                    { value.length >= 6 ? setValLength('green') : setValLength('grey') }
                    { value.match(/[0-9]/g) ? setValNum('green') : setValNum('grey') }
                }}
            />
            <View style={styles.space(5)} />
            <Input
                type="age"
                placeholder="age"
                value={valueAge}
                onChangeText={(value) => setValueAge(value)}
            />
            <View style={styles.space(24)} />
            <ValPassword valLength={valLength} valNum={valNum} />
            <View style={{ flex: 1 }} />
            
            <Button title='Sign Up' type="default" onPress={() => sendData()} />
            <View style={styles.garisBawah} />
            
        </View>
    )
}

export default SignUp

const styles = StyleSheet.create({
    gambar: {
        marginTop: 56,
    },
    text1: {
        fontSize: 22,
        fontWeight: 'bold',
        alignItems: 'center',
        color: colors.mainText
    },
    text2: {
        fontSize: 15,
        fontWeight: '300',
        alignItems: 'center',
        color: colors.secondaryText

    },
    text3: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.mainText
    },
    tombol: {
        alignItems: 'center'
    },
    space: value => {
        return {
            height: value,
        };
    },
    garisBawah: {
        marginTop: 35,
        width: 134,
        height: 5,
        backgroundColor: 'black',
        borderRadius: 3
    },
    signup: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.primary,
        marginLeft: 10
    }

})
