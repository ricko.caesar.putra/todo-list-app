import OnBoarding from "./OnBoarding";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import VerificationCode from "./VerificationCode"
import PasswordRecovery from "./PasswordRecovery"
import PasswordVerificationCode from "./PasswordVerificationCode"
import NewPassword from "./NewPassword"
import AllTask from "./AllTask"
import CompletedTask from "./CompletedTask"
import ActiveTask from "./ActiveTask"
import Profile from "./Profile"
import SplashScreen from "./SplashScreen"

export{OnBoarding,SignIn,SignUp,VerificationCode,PasswordRecovery,PasswordVerificationCode,NewPassword,AllTask,CompletedTask,ActiveTask,Profile,SplashScreen};


