import React, { useEffect, useState } from 'react'
import { Dimensions } from 'react-native';
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button, Input } from '../../components';
import { colors } from '../../utils';
const { height, width } = Dimensions.get('window')

const VerificationCode = ({ navigation }) => {
    let time = 300;
    // const [time, setTime] = useState(10)
    const [menit, setMenit] = useState(Math.floor(time / 60))
    const [detik, setDetik] = useState(time - Math.floor(time / 60) * 60)
    

    useEffect(() => {
        const interval = setInterval(() => {
            if (time >= 1) {
                time = time - 1
                // setTime(time-1)
                setMenit(Math.floor(time / 60))
                setDetik(time - Math.floor(time / 60) * 60)
                console.log(time)
            }
        }, 1e3);
        return () => clearInterval(interval);
    }, []);

    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <View style={styles.space(87)} />
            <Text style={styles.text1}>Check your email</Text>
            <View style={styles.space(8)} />
            <Text style={styles.text2}>We.ve sent the code to your email</Text>
            <View style={styles.space(32)} />
            <View style={{ width: width * 0.8, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Input type="verification" placeholder="" />
                <Input type="verification" placeholder="" />
                <Input type="verification" placeholder="" />
                <Input type="verification" placeholder="" />
            </View>
            <View style={styles.space(24)} />
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.text3}>Code expired in :  </Text>
                <Text style={{ color: 'red', fontSize: 15 }}>{time >= 1 ? `${menit}:${detik}` : 'kode expired'}</Text>
            </View>
            <View style={{ flex: 1 }} />
            <Button title='Verify' type="default" onPress={() => navigation.navigate('OnBoarding')} />
            <View style={styles.space(24)} />
            <Button title='Send again' type="default" white="true" onPress={() => navigation.navigate('OnBoarding')} />
            <View style={styles.space(24)} />
            <View style={styles.garisBawah} />
        </View>
    )
}

export default VerificationCode

const styles = StyleSheet.create({
    gambar: {
        marginTop: 56,
    },
    text1: {
        fontSize: 22,
        fontWeight: 'bold',
        alignItems: 'center',
        color: colors.mainText
    },
    text2: {
        fontSize: 15,
        fontWeight: '300',
        alignItems: 'center',
        color: colors.secondaryText

    },
    text3: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.mainText
    },
    tombol: {
        alignItems: 'center'
    },
    space: value => {
        return {
            height: value,
        };
    },
    garisBawah: {
        marginTop: 35,
        width: 134,
        height: 5,
        backgroundColor: 'black',
        borderRadius: 3
    },
    signup: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.primary,
        marginLeft: 10
    }

})
