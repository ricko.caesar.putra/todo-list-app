import React from 'react'
import { Dimensions } from 'react-native';
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button, Input } from '../../components';
import { colors } from '../../utils';
const { height, width } = Dimensions.get('window')

const PasswordRecovery = ({ navigation }) => {
    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <View style={styles.space(191)} />
            <Text style={styles.text1}>Password recovery</Text>
            <View style={styles.space(8)} />
            <Text style={styles.text2}>Enter your email to recover your password</Text>
            <View style={styles.space(32)} />
            <Input type="email" placeholder="Email or phone number" />
            <View style={styles.space(48)} />
            <View style={{ flex: 1 }} />
            <Button title='Sign In' type="default" onPress={() => navigation.navigate('PasswordVerificationCode')} />

            <View style={styles.garisBawah} />
        </View>
    )
}

export default PasswordRecovery

const styles = StyleSheet.create({
    gambar: {
        marginTop: 56,
    },
    text1: {
        fontSize: 22,
        fontWeight: 'bold',
        alignItems: 'center',
        color: colors.mainText
    },
    text2: {
        fontSize: 15,
        fontWeight: '300',
        alignItems: 'center',
        color: colors.secondaryText

    },
    text3: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.mainText
    },
    tombol: {
        alignItems: 'center'
    },
    space: value => {
        return {
            height: value,
        };
    },
    garisBawah: {
        marginTop: 35,
        width: 134,
        height: 5,
        backgroundColor: 'black',
        borderRadius: 3
    },
    signup: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.primary,
        marginLeft: 10
    }

})
