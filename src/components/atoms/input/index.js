import React, { useState } from 'react'
import { StyleSheet, Text, View,TextInput } from 'react-native'
import {TouchableOpacity } from 'react-native-gesture-handler'
import { colors } from '../../../utils'
import Icon from 'react-native-vector-icons/Ionicons'


const Input = ({ type, placeholder, notShowPassword, onPress, ...rest }) => {

    const [focusEmail,setFocusEmail]=useState(false)
    const [focusPassword,setFocusPassword]=useState(false)
    const [focusverification,setFocusverification]=useState(false)

    if (type == 'email'||type == 'name'||type == 'age'||type=='AddTask') {
        return (
            <View style={styles.wrapper(focusEmail)}>
                {type=='email'? <Icon name='ios-mail' size={20} color={'#2E3E5C'} />:null}
                {type=='name'?<Icon name='ios-person' size={20} color={'#2E3E5C'} />:null}
                {type=='age'?<Icon name='ios-hand-left' size={20} color={'#2E3E5C'} />:null}
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    placeholderTextColor={colors.outline}
                    onFocus={()=>setFocusEmail(true)}
                    onBlur={()=>setFocusEmail(false)}
                    {...rest}
                />
            </View>
        )
    }

    if (type == 'password') {
        return (
            <View style={styles.wrapper(focusPassword)}>
                <Icon name='ios-key' size={20} color='#2E3E5C' />
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    placeholderTextColor={colors.outline}
                    secureTextEntry={notShowPassword}
                    onFocus={()=>setFocusPassword(true)}
                    onBlur={()=>setFocusPassword(false)}
                    {...rest}
                />
                <TouchableOpacity style={{ paddingRight: 20 }} onPress={onPress}>
                    <Icon name='ios-eye' size={20} color={notShowPassword?'#2E3E5C':'green'} />
                </TouchableOpacity>
            </View>
        )
    }

    if (type == 'verification') {
        return (
            <View style={styles.wrapperVer(focusverification)}>
                <TextInput
                    style={styles.textInputVar}
                    placeholder={placeholder}
                    placeholderTextColor={colors.outline}
                    maxLength={1}
                    onFocus={()=>setFocusverification(true)}
                    onBlur={()=>setFocusverification(false)}
                    {...rest}
                />
            </View>
        )
    }

}

export default Input


const styles = {
    wrapper: (focus)=>{ 
        return{
        flexDirection: 'row',
        width: 300,
        height: 56,
        borderWidth: 2,
        borderColor: focus? '#1FCC79':colors.outline,
        borderRadius: 32,
        paddingLeft: 26,
        alignItems: 'center'
    }},
    wrapperVer:(focus)=>{
        return{
        width: 72,
        height: 72,
        borderWidth: 2,
        borderColor: focus?'#1FCC79':colors.outline,
        borderRadius: 20,
        alignItems: 'center'
        }
    },
    textInput: {
        flex: 1,
        fontSize: 15,
        marginLeft: 13
    },
    textInputVar:{
        fontSize: 32,
        alignContent:'center',
        justifyContent:'center'
    }
}
