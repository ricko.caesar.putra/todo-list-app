import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import LottieView from 'lottie-react-native';
import { OkIcon, XIcon } from '../../assets';
import { Input } from '.';
import { colors } from '../../utils';

const CardProfile = ({ age, name, email, createdAt, updatedAt, activeTask, openTask, }) => {
    return (
        <View style={styles.itemContainer}>
            <View style={{ flexDirection: 'row', alignItems: 'center', alignContent: 'space-around' }}>
                <View style={styles.task}>
                    <Text style={styles.createdAt}>All</Text>
                    <Text style={{fontSize:25,fontWeight:'bold'}}>{activeTask}</Text>
                </View>
                <Image
                    source={{ uri: `https://i.pravatar.cc/150?u=${createdAt}` }}
                    style={styles.avatar} />
                                <View style={styles.task}>
                    <Text style={styles.createdAt}>Open</Text>
                    <Text style={{fontSize:25,fontWeight:'bold'}}>{openTask}</Text>
                </View>
            </View>

            <View style={styles.desc}>
            <Input
                type="name"
                placeholder="name"
                value={name}
                editable={false}
            />
            <View style={{ height: 5 }} />
            <Input
                type="email"
                placeholder="Email"
                value={email}
                editable={false}
            />
            <View style={{ height: 5 }} />
            <Input
                type="age"
                placeholder="age"
                value={''+age}
                editable={false}
            />

                
                {/* <Text style={styles.description}>name: {name}</Text>
                <Text style={styles.description}>age: {age}</Text>
                <Text style={styles.description}>email:{email}</Text> */}
                <View style={{ height: 10 }} />
                {/* <Text style={styles.createdAt}>created At: {createdAt}</Text>
                    <Text style={styles.updatedAt}>updated At: {updatedAt}</Text> */}
            </View>
            <View style={{ height: 40 }} />


        </View>
    )
}

export default CardProfile

const styles = StyleSheet.create({
    itemContainer: {
        height: 430,
        backgroundColor: "white",
        // marginBottom: 5,
        borderRadius: 40,
        alignItems: "center",
        borderColor: colors.primary,
        borderWidth: 3,
        padding: 5,
        elevation: 10,
    },
    avatar: {
        width: 150,
        height: 150,
        borderRadius: 20,
        marginVertical: 10
    },
    ChangeStat: {
        width: 150,
        fontSize: 15,
        fontWeight: 'bold',
        color: 'blue',
        padding: 5,
        margin: 10,
        borderColor: 'green',
        borderRadius: 15,
        borderWidth: 4,
        textAlign: 'center'
    },
    delete: {
        width: 150,
        fontSize: 15,
        fontWeight: 'bold',
        color: 'red',
        padding: 5,
        margin: 10,
        borderColor: 'green',
        borderRadius: 15,
        borderWidth: 4,
        textAlign: 'center'
    },
    desc: { marginLeft: 18, flex: 1 },
    status: (completed) => {
        return {
            fontSize: 15,
            fontWeight: 'bold',
            marginBottom: 10,
            color: completed ? 'green' : 'red'
        }
    },
    description: { fontSize: 20, fontWeight: 'bold' },
    owner: { fontSize: 20, fontWeight: 'bold' },
    createdAt: { fontSize: 14, fontWeight: 'bold',marginVertical:6 },
    updatedAt: { fontSize: 12, fontWeight: 'bold' },
    task: {
        alignItems: 'center',
        borderWidth: 3,
        borderColor: colors.primary,
        margin: 20,
        width: 90,
        height: 90,
        borderRadius: 45,
        padding: 5
    }
})
