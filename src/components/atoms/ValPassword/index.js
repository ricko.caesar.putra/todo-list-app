import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../../../utils';

const ValPassword = ({valLength,valNum}) => {
    return (
        <View>
            <Text style={styles.text3}>Your Password must Contain:</Text>
            <View style={{ flexDirection: 'row', marginTop: 16 }}>
                <View style={{ width: 25, height: 25, backgroundColor: valLength, borderRadius: 20, fontWeight: 'bold', justifyContent: 'center', alignItems: 'center' }}>
                    <Icon name='ios-checkmark' size={15} color={'white'} />
                </View>
                <Text style={{ fontWeight: 'bold', marginLeft: 10,color:valLength }}>Atleast 6 charracters</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 16 }}>
                <View style={{ width: 25, height: 25, backgroundColor:valNum, borderRadius: 20, fontWeight: 'bold', justifyContent: 'center', alignItems: 'center' }}>
                    <Icon name='ios-checkmark' size={15} color={'white'} />
                </View>
                <Text style={{ fontWeight: 'bold', marginLeft: 10, color:valNum }}>contain a number</Text>
            </View>
        </View>
    )
}

export default ValPassword

const styles = StyleSheet.create({
    text3: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.mainText
    },
})
