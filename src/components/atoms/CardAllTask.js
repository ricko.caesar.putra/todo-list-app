import React, { useState } from 'react'
import { Image, StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import LottieView from 'lottie-react-native';
import { OkIcon, XIcon } from '../../assets';
import { colors } from '../../utils';
import moment from 'moment';

const CardAllTask = ({ completed, description, owner, createdAt, updatedAt, onPress, onDelete }) => {
    const [isCardShow, SetIsCardShow] = useState(true)

    const onDeleteCard = () => {
        SetIsCardShow(false)
        onDelete()
        setTimeout(() => {
            SetIsCardShow(true)
        }, 2000);

    }

    const onPressCard = () => {
        SetIsCardShow(false)
        onPress()
        setTimeout(() => {
            SetIsCardShow(true)
        }, 3000);

    }

    return (
        <View style={styles.itemContainer}>
            {!isCardShow ?
                <ActivityIndicator style={{ marginTop: 10 }} size='small' color={'red'} />
                :
                <View style={{ flexDirection: 'row' }}>
                    {/* <Image source={{ uri: `https://i.pravatar.cc/150?u=${description}` }}
                style={styles.avatar} /> */}
                    {completed ? <LottieView source={OkIcon} style={{ width: 100, height: 100 }} autoPlay loop /> : <LottieView source={XIcon} style={{ width: 100, height: 100, }} autoPlay loop />}

                    <View style={styles.desc}>
                        <Text style={styles.description}>{description}</Text>
                        <Text style={styles.status(completed)}>Status: {completed ? 'close' : 'open'}</Text>
                        {/* <Text style={styles.owner}>owner: {owner}</Text> */}
                        <Text style={styles.createdAt}>created: {moment(createdAt, "YYYYMMDD").format('MMMM Do YYYY, h:mm:ss a')}</Text>
                        <Text style={styles.updatedAt}>updated: {moment(createdAt, "YYYYMMDD").format('MMMM Do YYYY, h:mm:ss a')}</Text>
                    </View>
                </View>}

            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                <TouchableOpacity onPress={onPressCard}>
                    <Text style={styles.ChangeStat}>Change Status</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={onDeleteCard}>
                    <Text style={styles.delete}>Delete</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default CardAllTask

const styles = StyleSheet.create({
    itemContainer: {
        backgroundColor: "white",
        marginBottom: 5,
        borderRadius: 20,
        alignItems: "center",
        borderColor: colors.primary,
        borderWidth: 3,
        padding: 5,
        elevation: 10,
    },
    avatar: {
        width: 100,
        height: 100,
        borderRadius: 20
    },
    ChangeStat: {
        width: 150,
        fontSize: 15,
        fontWeight: 'bold',
        color: 'blue',
        padding: 5,
        margin: 10,
        borderColor: colors.primary,
        borderRadius: 15,
        borderWidth: 4,
        textAlign: 'center'
    },
    delete: {
        width: 150,
        fontSize: 15,
        fontWeight: 'bold',
        color: 'red',
        padding: 5,
        margin: 10,
        borderColor: colors.primary,
        borderRadius: 15,
        borderWidth: 4,
        textAlign: 'center'
    },
    desc: { marginLeft: 18, flex: 1 },
    status: (completed) => {
        return {
            fontSize: 15,
            fontWeight: 'bold',
            marginBottom: 10,
            color: completed ? 'green' : 'red'
        }
    },
    description: { fontSize: 20, fontWeight: 'bold' },
    owner: { fontSize: 20, fontWeight: 'bold' },
    createdAt: { fontSize: 12, fontWeight: 'bold' },
    updatedAt: { fontSize: 12, fontWeight: 'bold' },
})
