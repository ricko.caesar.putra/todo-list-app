import React, { useEffect, useState } from 'react'
import { Dimensions } from 'react-native';
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Button, Input, ValPassword } from '../../components';
import { colors } from '../../utils';
const { height, width } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons'
import axios from 'axios'


const CardEditProfile = ({ token, age, name, email }) => {
    const [notShowPassword, setnotShowPassword] = useState(true)
    const [valueName, setValueName] = useState(name)
    const [valuePassword, setValuePassword] = useState('')
    const [valueEmail, setValueEmail] = useState(email)
    const [valueAge, setValueAge] = useState(''+age)
    const [valNum, setValNum] = useState('grey')
    const [valLength, setValLength] = useState('grey')

    const baseUrl = 'https://api-nodejs-todolist.herokuapp.com/user/me';
    const headers = {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
    }

    const sendData = () => {
        axios({
            method: 'put',
            url: baseUrl,
            headers,
            data: { 
                "age": valueAge,
                "name": valueName,
                "email": valueEmail,
                "password": valuePassword,
        },
        })
            .catch(error => {
                console.warn(error.response)
            })
    }

    return (
        <View style={styles.itemContainer}>
            <Input
                type="name"
                placeholder="name"
                value={valueName}
                onChangeText={(value) => setValueName(value)}
            />
            <View style={styles.space(5)} />
            <Input
                type="email"
                placeholder="Email"
                value={valueEmail}
                onChangeText={(value) => setValueEmail(value)}
            />
            <View style={styles.space(5)} />
            <Input
                type="password"
                placeholder="Password"
                notShowPassword={notShowPassword}
                onPress={() => setnotShowPassword(!notShowPassword)}
                value={valuePassword}
                onChangeText={(value) => {
                    setValuePassword(value)
                    { value.length >= 6 ? setValLength('green') : setValLength('grey') }
                    { value.match(/[0-9]/g) ? setValNum('green') : setValNum('grey') }
                }}
            />
            <View style={styles.space(5)} />
            <Input
                type="age"
                placeholder="age"
                value={valueAge}
                onChangeText={(value) => setValueAge(value)}
            />
            <View style={styles.space(5)} />
            <ValPassword valLength={valLength} valNum={valNum} />
            <View style={{height:5}}/>
            <Button title='Confirm' type="default" onPress={() => sendData()} />
            
        </View>
    )
}

export default CardEditProfile

const styles = StyleSheet.create({
    itemContainer: {
        height:430,
        backgroundColor: "white",
        // marginBottom: 5,
        borderRadius: 40,
        alignItems: "center",
        borderColor: colors.primary,
        borderWidth: 3,
        padding: 5,
        elevation: 10,
    },
    gambar: {
        marginTop: 56,
    },
    text1: {
        fontSize: 22,
        fontWeight: 'bold',
        alignItems: 'center',
        color: colors.mainText
    },
    text2: {
        fontSize: 15,
        fontWeight: '300',
        alignItems: 'center',
        color: colors.secondaryText

    },
    text3: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.mainText
    },
    tombol: {
        alignItems: 'center'
    },
    space: value => {
        return {
            height: value,
        };
    },
    garisBawah: {
        marginTop: 35,
        width: 134,
        height: 5,
        backgroundColor: 'black',
        borderRadius: 3
    },
    signup: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: '500',
        color: colors.primary,
        marginLeft: 10
    }

})
