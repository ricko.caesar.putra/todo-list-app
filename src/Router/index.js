import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { View } from 'react-native'
import { createStackNavigator } from "@react-navigation/stack";
import { SplashScreen, OnBoarding, SignIn, SignUp, VerificationCode, PasswordRecovery, PasswordVerificationCode, NewPassword, AllTask, ActiveTask, CompletedTask, Profile } from '../pages';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { colors } from '../utils';
import Icon from 'react-native-vector-icons/Ionicons'

const MaterialBottom = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator initialRouteName="SplashScreen">
            <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="OnBoarding"
                component={OnBoarding}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="SignIn"
                component={SignIn}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="SignUp"
                component={SignUp}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="VerificationCode"
                component={VerificationCode}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="PasswordRecovery"
                component={PasswordRecovery}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="PasswordVerificationCode"
                component={PasswordVerificationCode}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen name="BotomTab"
                component={BotomTab}
                options={{
                    title: "BotomTab",
                    headerShown: false
                }} />
            <Stack.Screen
                name="NewPassword"
                component={NewPassword}
                options={{
                    headerShown: false,
                }}
            />
            {/* <Stack.Screen
                name="AllTask"
                component={AllTask}
                options={{
                    headerShown: false,
                }}
            /> */}

        </Stack.Navigator>
    )
}

const BotomTab = () => {
    return (
        <MaterialBottom.Navigator
            shifting={false}
            initialRouteName="AllTask"
            barStyle={{
                backgroundColor: "white",
                borderWidth: .5,
                borderColor: colors.primary
            }}
            activeColor={colors.secondary}
            inactiveColor='black'
        >

            <MaterialBottom.Screen name="AllTask" component={AllTask} options={{
                tabBarLabel: 'AllTask',
                tabBarIcon: ({ color }) => (
                    <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        {/* <Image
                            source={ImageAccount}
                            style={{ width: 20, height: 20 }}
                        /> */}
                        <Icon name='ios-list' size={20} color='white' />
                    </View>
                ),
                tabBarBadge: 1,
                tabBarColor: "red",
            }}
            />

            <MaterialBottom.Screen name="ActiveTask" component={ActiveTask} options={{
                tabBarLabel: 'ActiveTask',
                tabBarIcon: ({ color }) => (
                    <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        {/* <Image
                            source={ImageAccount}
                            style={{ width: 20, height: 20 }}
                        /> */}
                        <Icon name='ios-list-circle-outline' size={20} color='white' />
                    </View>
                ),
                tabBarBadge: 1,
                tabBarColor: "red",
            }}
            />
            <MaterialBottom.Screen name="CompletedTask" component={CompletedTask} options={{
                tabBarLabel: 'CompletedTask',
                tabBarIcon: ({ color }) => (
                    <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        {/* <Image
                            source={ImageAccount}
                            style={{ width: 20, height: 20 }}
                        /> */}
                        <Icon name='ios-list-circle' size={20} color='white' />
                    </View>
                ),
                tabBarBadge: 1,
                tabBarColor: "red",
            }}
            />
            <MaterialBottom.Screen name="Profile" component={Profile} options={{
                tabBarLabel: 'Profile',
                tabBarIcon: ({ color }) => (
                    <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        {/* <Image
                            source={ImageAccount}
                            style={{ width: 20, height: 20 }}
                        /> */}
                        <Icon name='ios-person' size={20} color='white' />
                    </View>
                ),
                tabBarBadge: 1,
                tabBarColor: "red",
            }}
            />
        </MaterialBottom.Navigator>
    )

}


export default class Router extends Component {
    render() {
        return (
            <NavigationContainer>
                <HomeStack />
            </NavigationContainer>
        )
    }
}