import { combineReducers } from "redux"

export const initialStateLogin = {
    form: {
        age: '',
        _id: '',
        email: '',
        createdAt: '',
        updatedAt: '',
        __v:''
    },
    title: 'Login Page',
    desc: 'ini adalah desc untuk Login'
}

const RegisterReducer = (state = initialStateLogin, action) => {
    if (action.type === 'SET_FORM') {
        return {
            ...state,
            form: {
                ...state.form,
                [action.inputType]: action.inputValue,
            }
        }
    }
    return state
}


const reducer = combineReducers({
    RegisterReducer,
})

export default reducer;