import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Router from './Router';
// import { Provider } from 'react-redux'
// import { store } from './redux';
import { Provider } from 'react-redux';
import store from './stores';


const App = () => {
  return (

    // <Provider store={store}>
    //   <NavigationContainer>
    //     <Router />
    //   </NavigationContainer>
    // </Provider>
    <Provider store={store}>
      <Router />
    </Provider>
  );
};

const styles = StyleSheet.create({

});

export default App;
