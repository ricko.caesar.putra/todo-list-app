import { combineReducers } from 'redux';

import authStoreReducer from './authStoreReducer';
import customerStoreReducer from './customerStoreReducer';

const reducers = {
  authStore: authStoreReducer,
  customerReducer: customerStoreReducer,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
