// images
import OnboardingImage from './images/Onboarding.png';
import AnimationList from './images/39603-list-and-grid.json';
import AnimationList2 from './images/10577-list.json';
import OkIcon from './images/27171-ok-icon.json';
import XIcon from './images/27172-red-x-icon.json';



export {OnboardingImage,AnimationList,AnimationList2,OkIcon,XIcon}
